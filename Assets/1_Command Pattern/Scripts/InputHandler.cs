﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [Tooltip("Character that we want to use this on.")]
    public GameObject actor;
    Animator anim;

    Command keyQ, KeyW, keyE, upArrow;
    List<Command> oldCommands = new List<Command>();

    Coroutine replayCoroutine;
    bool shouldStartReplay;
    bool isReplaying;

    void Start()
    {
        anim = actor.GetComponent<Animator>();

        //Each command can do 1 of 2 things: Perform jump or Do nothing.
        //Define what each command does:
        keyQ = new PerformJump();
        KeyW = new PerformPunch();
        keyE = new PerformKick();
        upArrow = new MoveForward();

        //Set camera to any character depending which is assigned as player.
        Camera.main.GetComponent<CameraFollow360>().player = actor.transform;
    }

    void Update()
    {
        if(!isReplaying)
        {
            HandleInput();
        }
        StartReplay();
    }

    void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            keyQ.Execute(anim, true);
            oldCommands.Add(keyQ);
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            KeyW.Execute(anim, true);
            oldCommands.Add(KeyW);
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            keyE.Execute(anim, true);
            oldCommands.Add(keyE);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            upArrow.Execute(anim, true);
            oldCommands.Add(upArrow);
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            shouldStartReplay = true;
        }

        if(Input.GetKeyDown(KeyCode.Z))
        {
            UndoLastCommand();
        }
    }

    void UndoLastCommand()
    {
        //To avoid argument outof range error.
        if(oldCommands.Count > 0)
        {
            Command c = oldCommands[oldCommands.Count - 1];
            c.Execute(anim, false);
            oldCommands.RemoveAt(oldCommands.Count - 1);
        }
    }
    
    void StartReplay()
    {
        if(shouldStartReplay && oldCommands.Count > 0)
        {
            shouldStartReplay = false;

            if(replayCoroutine != null)
            {
                StopCoroutine(replayCoroutine);
            }
            replayCoroutine = StartCoroutine(ReplayCommands());
        }
    }

    IEnumerator ReplayCommands()
    {
        isReplaying = true;

        for(int i = 0; i < oldCommands.Count; i++)
        {
            oldCommands[i].Execute(anim, true);
            yield return new WaitForSeconds(1f);
        }

        isReplaying = false;
    }
}