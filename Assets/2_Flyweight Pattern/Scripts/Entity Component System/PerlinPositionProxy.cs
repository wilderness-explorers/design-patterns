﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

//[RequiresEntityConversion]
public class PerlinPositionProxy : MonoBehaviour, IConvertGameObjectToEntity
{
    //Entity manager is pool of all the entities.
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        //Creating a new component.
        var data = new PerlinPosition { };

        //Adding the created component to the entity (Cube).
        //Only way to find entities, is by the components attached to them.
        dstManager.AddComponentData(entity, data);
    }
}