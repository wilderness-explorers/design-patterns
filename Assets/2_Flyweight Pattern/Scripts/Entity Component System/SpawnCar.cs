﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCar : MonoBehaviour
{
    public GameObject car;
    public GameObject carCamera;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Vector3 pos = new Vector3(10, 10, 10);
            GameObject carInstance = Instantiate(car, pos, Quaternion.identity);
            carCamera.GetComponent<SmoothFollow>().target = carInstance.transform;
        }
    }
}
