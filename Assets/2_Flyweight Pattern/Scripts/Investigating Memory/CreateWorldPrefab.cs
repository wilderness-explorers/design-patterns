﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateWorldPrefab : MonoBehaviour
{
    [Tooltip("Width and depth of the landscape to be created with cubes.")]
    public int width, depth;
    public GameObject cube;
    
    void Start()
    {
        for(int x = 0; x < width; x++)
        {
            for(int z = 0; z < depth; z++)
            {
                Vector3 pos = new Vector3(x, Mathf.PerlinNoise(x * 0.2f, z * 0.2f) * 3, z);
                GameObject obj = Instantiate(cube, pos, Quaternion.identity);
            }
        }
    }

    void Update()
    {
        
    }
}
