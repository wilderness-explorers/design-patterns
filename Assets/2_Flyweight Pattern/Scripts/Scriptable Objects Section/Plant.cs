﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Plant : MonoBehaviour
{
    [SerializeField]
    private PlantData plantData;

    SetPlantInfo setPlantInfo;

    private void Start()
    {
        setPlantInfo = GameObject.FindGameObjectWithTag("PlantInfo").GetComponent<SetPlantInfo>();
    }

    private void OnMouseDown()
    {
        setPlantInfo.OpenPlantPanel();
        setPlantInfo.plantName.text = plantData.PlantName;
        setPlantInfo.threatLevel.text = plantData.PlantThreat.ToString();
        setPlantInfo.plantIcon.GetComponent<RawImage>().texture = plantData.Icon;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (plantData.PlantThreat == PlantData.THREAT.High)
            {
                PlayerControllerCopy.dead = true;
            }
        }
    }
}