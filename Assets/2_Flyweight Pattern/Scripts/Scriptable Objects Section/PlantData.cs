﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "plantdata", menuName = "Plant Data", order = 51)]
public class PlantData : ScriptableObject
{
    public enum THREAT { None, Low, Moderate, High }

    [SerializeField]
    private string plantName;
    public string PlantName
    {
        get
        {
            return plantName;
        }
    }

    [SerializeField]
    private THREAT plantThreat;
    public THREAT PlantThreat
    {
        get
        {
            return plantThreat;
        }
    }

    [SerializeField]
    private Texture icon;
    public Texture Icon
    {
        get
        {
            return icon;
        }
    }
}