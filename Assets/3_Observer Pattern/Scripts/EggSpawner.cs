using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject eggPrefab;
    [SerializeField]
    private Terrain terrain;
    private TerrainData terrainData;
    public Event eggDrop;

    void Start()
    {
        terrainData = terrain.terrainData;

        InvokeRepeating("CreateEgg", 1, 0.1f);
    }

    private void CreateEgg()
    {
        //Random position within the extent of the terrain.
        int x = (int)Random.Range(0, terrainData.size.x);
        int z = (int)Random.Range(0, terrainData.size.z);

        Vector3 spawnPosition = new Vector3(x, 0, z);
        spawnPosition.y = terrain.SampleHeight(spawnPosition) + 10;

        GameObject egg = Instantiate(eggPrefab, spawnPosition, Quaternion.identity);

        eggDrop.Occured(egg);
    }
}