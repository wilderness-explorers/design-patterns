using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewEvent", menuName = "Game Event", order = 52)]
public class Event : ScriptableObject
{
    //List of event listeners.
    private List<EventListener> eventListeners = new List<EventListener>();

    public void RegisterListener(EventListener eventListener)
    {
        eventListeners.Add(eventListener);
    }

    public void UnRegisterListener(EventListener eventListener)
    {
        eventListeners.Remove(eventListener);
    }

    //Determines what will happen, when an event occurs.
    public void Occured(GameObject gameObject)
    {
        for(int i = 0; i < eventListeners.Count; i++)
        {
            eventListeners[i].OnEventOccurs(gameObject);
        }
    }
}