using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityGameObjectEvent : UnityEvent<GameObject>
{

}

public class EventListener : MonoBehaviour
{
    public Event gEvent;
    public UnityGameObjectEvent response = new UnityGameObjectEvent();

    public void OnEnable()
    {
        gEvent.RegisterListener(this);
    }

    public void OnDisable()
    {
        gEvent.UnRegisterListener(this);
    }

    public void OnEventOccurs(GameObject gameObject)
    {
        response.Invoke(gameObject);
    }
}