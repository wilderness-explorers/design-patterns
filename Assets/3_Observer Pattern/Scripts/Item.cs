using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    public Event itemDropped;
    public Event itemPicked;
    public Image itemIcon;

    void Start()
    {
        itemDropped.Occured(this.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            itemPicked.Occured(this.gameObject);

            //Hide immediately.
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            this.gameObject.GetComponent<Collider>().enabled = false;

            //Destroy after few seconds - Radar script to finish.
            Destroy(this.gameObject, 5);
        }
    }
}
