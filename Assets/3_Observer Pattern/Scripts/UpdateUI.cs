using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpdateUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI pickupStatus;
    public float displayTime = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        pickupStatus.enabled = false;
    }

    public void DisplayStatus()
    {
        pickupStatus.enabled = true;
        pickupStatus.text = "You picked up an item.";
        Invoke("HideStatus", displayTime);
    }

    public void HideStatus()
    {
        pickupStatus.enabled = false;
    }
}
