using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour
{
    void Update()
    {
        if (Random.Range(0, 100) < 10)
        {
            //Creates new cube mesh for every instance.
            //ProceduralCube.CreateCube(this.transform.position);

            //Every instance shares the same mesh and its components.
            ProceduralCube.Clone(this.transform.position);
        }
    }
}
