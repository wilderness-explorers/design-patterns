using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereSpawner : MonoBehaviour
{
    void Update()
    {
        if (Random.Range(0, 100) < 10)
        {
            //Creates new cube mesh for every instance.
            //ProceduralSphere.CreateSphere(this.transform.position);

            //Every instance shares the same mesh and its components.
            ProceduralSphere.Clone(this.transform.position);
        }
    }
}
