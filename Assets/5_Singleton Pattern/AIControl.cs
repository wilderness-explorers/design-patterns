﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace SingletonPattern
{
    public class AIControl : MonoBehaviour
    {
        //GameObject[] goalLocations;
        UnityEngine.AI.NavMeshAgent agent;
        Animator anim;
        Vector3 lastGoal;

        // Use this for initialization
        void Start()
        {
            //goalLocations = GameObject.FindGameObjectsWithTag("goal");
            agent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
            anim = GetComponent<Animator>();
            anim.SetBool("isWalking", true);
            PickGoalLocation();
        }

        void PickGoalLocation()
        {
            lastGoal = agent.destination;
            GameObject goalPosition = GameEnvironment.Singleton.GetRandomGoal();
            agent.SetDestination(goalPosition.transform.position);
        }

        // Update is called once per frame
        void Update()
        {
            if (agent.remainingDistance < 1) //At the goal
            {
                PickGoalLocation();
            }

            foreach(GameObject obj in GameEnvironment.Singleton.Obstacles)
            {
                float distance = Vector3.Distance(obj.transform.position, this.transform.position);

                //If distance between player and garbage game object is less than a value, player returns back to previous goal.
                if (distance < 5 && Random.Range(0, 100) < 5)
                {
                    agent.SetDestination(lastGoal);
                }
                else if(distance < 1)
                {
                    GameEnvironment.Singleton.RemoveObstacles(obj);
                    break;
                }
            }
        }
    }
}