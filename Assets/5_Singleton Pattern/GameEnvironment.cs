using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Sealed classes cannot be inherited by other classes.
public sealed class GameEnvironment
{
    private static GameEnvironment instance;

    private List<GameObject> obstacles = new List<GameObject>();
    public List<GameObject> Obstacles
    {
        get
        {
            return obstacles;
        }
    }

    private List<GameObject> goals = new List<GameObject>();
    public List<GameObject> Goals
    {
        get
        {
            return goals;
        }
    }

    public static GameEnvironment Singleton
    {
        get
        {
            if(instance == null)
            {
                instance = new GameEnvironment();
                instance.Goals.AddRange(GameObject.FindGameObjectsWithTag("goal"));
            }

            return instance;
        }
    }

    public GameObject GetRandomGoal()
    {
        int index = Random.Range(0, goals.Count);
        return goals[index];
    }

    //To add obstacles from the list.
    public void AddObstacles(GameObject obj)
    {
        obstacles.Add(obj);
    }

    //To remove obstacles from the list.
    public void RemoveObstacles(GameObject obj)
    {
        int index = obstacles.IndexOf(obj);
        obstacles.RemoveAt(index);
        GameObject.Destroy(obj);
    }
}
