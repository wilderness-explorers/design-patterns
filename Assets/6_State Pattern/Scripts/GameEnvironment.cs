using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace StatePattern
{
    public class GameEnvironment : MonoBehaviour
    {
        private static GameEnvironment instance;
        private List<GameObject> checkpoints = new List<GameObject>();
        public List<GameObject> Checkpoints
        {
            get
            {
                return checkpoints;
            }
        }

        public static GameEnvironment Singleton
        {
            get
            {
                if(instance == null)
                {
                    instance = new GameEnvironment();

                    //Finds waypoints in random order.
                    instance.Checkpoints.AddRange(GameObject.FindGameObjectsWithTag("Checkpoint"));

                    //Ordered by waypoint's name. Ascending alphabetical order.
                    instance.checkpoints = instance.checkpoints.OrderBy(waypoint => waypoint.name).ToList();
                }

                return instance;
            }
        }
    }
}