using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace StatePattern
{
    public class State
    {
        public enum STATE
        {
            IDLE, PATROL, PURSUE, ATTACK, SLEEP
        };

        //To indicate progress of a particular state.
        public enum EVENT
        {
            ENTER, UPDATE, EXIT
        };

        public STATE name;
        protected EVENT stage;

        protected GameObject npc;
        protected NavMeshAgent agent;
        protected Animator anim;
        protected Transform player;

        protected State nextState;

        float visDistance = 10.0f;
        float visAngle = 30.0f;
        float shootDistance = 7.0f;

        //Constructor.
        public State(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player)
        {
            npc = _npc;
            agent = _agent;
            anim = _anim;
            player = _player;
        }

        //Skeleton code for each of the different phases of a state.
        public virtual void Enter()
        {
            //Once in the entry phase, the next phase should be set which is Update.
            stage = EVENT.UPDATE;
        }

        public virtual void Update()
        {
            //Once in update phase, the phase needs to maintained until update kicks it out.
            stage = EVENT.UPDATE;
        }

        public virtual void Exit()
        {
            stage = EVENT.EXIT;
        }

        //Method that will be run from outside, that progresses through each of the different phases.
        public State Process()
        {
            if (stage == EVENT.ENTER)
            {
                Enter();
            }

            if (stage == EVENT.UPDATE)
            {
                Update();
            }

            if (stage == EVENT.EXIT)
            {
                Exit();
                return nextState;
            }

            return this;
        }

        //To chase the player.
        public bool CanSeePlayer()
        {
            Vector3 direction = player.position - npc.transform.position;
            float angle = Vector3.Angle(direction, npc.transform.forward);

            //visAngle needs to be half of desired field of view of NPC.
            if (direction.magnitude < visDistance && angle < visAngle)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //To check if npc is close enough to attack the player.
        public bool CanAttackPlayer()
        {
            Vector3 direction = player.position - npc.transform.position;

            if (direction.magnitude < shootDistance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class Idle : State
    {
        //Constructor
        public Idle(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player) : base(_npc, _agent, _anim, _player)
        {
            name = STATE.IDLE;
        }

        public override void Enter()
        {
            anim.SetTrigger("isIdle");
            base.Enter();
        }

        public override void Update()
        {
            if(CanSeePlayer())
            {
                nextState = new Pursue(npc, agent, anim, player);
                stage = EVENT.EXIT;
            }
            else if (Random.Range(0, 100) < 10)
            {
                nextState = new Patrol(npc, agent, anim, player);
                stage = EVENT.EXIT;
            }
        }

        public override void Exit()
        {
            anim.ResetTrigger("isIdle");
            base.Exit();
        }
    }

    public class Patrol : State
    {
        //For the state to move the character around waypoints.
        int currentIndex = -1;

        //Constructor.
        public Patrol(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player) : base(_npc, _agent, _anim, _player)
        {
            name = STATE.PATROL;
            agent.speed = 2;
            agent.isStopped = false;
        }

        public override void Enter()
        {
            float lastDist = Mathf.Infinity;
            for (int i = 0; i < GameEnvironment.Singleton.Checkpoints.Count; i++)
            {
                GameObject thisWayPoint = GameEnvironment.Singleton.Checkpoints[i];
                float distance = Vector3.Distance(npc.transform.position, thisWayPoint.transform.position);

                if(distance < lastDist)
                {
                    //Decreased by 1 as it gets incremented again in update before set destination.
                    currentIndex = i - 1;
                    lastDist = distance;
                }
            }
            anim.SetTrigger("isWalking");
            base.Enter();
        }

        public override void Update()
        {
            if (agent.remainingDistance < 1)
            {
                if (currentIndex >= GameEnvironment.Singleton.Checkpoints.Count - 1)
                {
                    //Reset destination.
                    currentIndex = 0;
                }
                else
                {
                    currentIndex++;
                }

                agent.SetDestination(GameEnvironment.Singleton.Checkpoints[currentIndex].transform.position);
            }

            if (CanSeePlayer())
            {
                nextState = new Pursue(npc, agent, anim, player);
                stage = EVENT.EXIT;
            }
        }

        public override void Exit()
        {
            anim.ResetTrigger("isWalking");
            base.Exit();
        }
    }

    public class Pursue : State
    {
        public Pursue(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player) : base(_npc, _agent, _anim, _player)
        {
            name = STATE.PURSUE;
            agent.speed = 5;
            agent.isStopped = false;
        }

        public override void Enter()
        {
            anim.SetTrigger("isRunning");
            base.Enter();
        }

        public override void Update()
        {
            agent.SetDestination(player.position);
            base.Update();
            if(agent.hasPath)
            {
                if(CanAttackPlayer())
                {
                    nextState = new Attack(npc, agent, anim, player);
                    stage = EVENT.EXIT;
                }
                else if(!CanSeePlayer())
                {
                    nextState = new Patrol(npc, agent, anim, player);
                    stage = EVENT.EXIT;
                }
            }
        }

        public override void Exit()
        {
            anim.ResetTrigger("isRunning");
            base.Exit();
        }
    }

    public class Attack : State
    {
        float rotationSpeed = 2.0f;
        AudioSource shoot;

        public Attack (GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player) : base(_npc, _agent, _anim, _player)
        {
            name = STATE.ATTACK;
            shoot = _npc.GetComponent<AudioSource>();
        }

        public override void Enter()
        {
            anim.SetTrigger("isShooting");
            agent.isStopped = true;
            shoot.Play();
            base.Enter();
        }

        public override void Update()
        {
            //Rotate the npc towards player to start shooting.
            Vector3 direction = player.position - npc.transform.position;
            float angle = Vector3.Angle(direction, npc.transform.forward);
            direction.y = 0;

            npc.transform.rotation = Quaternion.Slerp(npc.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);

            //If player has gone out of range.
            if(!CanAttackPlayer())
            {
                nextState = new Idle(npc, agent, anim, player);
                stage = EVENT.EXIT;
            }
        }

        public override void Exit()
        {
            anim.ResetTrigger("isShooting");
            shoot.Stop();
            base.Exit();
        }
    }
}