using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInvisible : MonoBehaviour
{
    private void OnEnable()
    {
        //Debug.LogError("Bullet instantiated");
    }

    private void OnBecameInvisible()
    {
        //Debug.LogError("Became invisible");
        //Destroy(this.gameObject);

        this.gameObject.SetActive(false);
    }
}
