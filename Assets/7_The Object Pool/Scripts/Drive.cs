﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Drive : MonoBehaviour
{
    public float speed = 10.0f;
    //public GameObject bullet;

    public Slider healthBar;

    void Update()
    {
        float translation = Input.GetAxis("Horizontal") * speed;
        translation *= Time.deltaTime;
        transform.Translate(translation, 0, 0);

        if(Input.GetKeyDown("space"))
        {
            //Instantiate(bullet, this.transform.position, Quaternion.identity);

            GameObject bulletPrefab = Pool.singleton.Get("Bullet");

            if(bulletPrefab != null)
            {
                bulletPrefab.transform.position = this.transform.position;
                bulletPrefab.SetActive(true);
            }
        }

        //Get position of ship in world coordinates.
        //To convert to canvas coordinates from world coordinates, as Slider in Canvas works in Canvas coordinates.
        Vector3 screenPos = Camera.main.WorldToScreenPoint(this.transform.position) + new Vector3(0, -140, 0);
        healthBar.transform.position = screenPos;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Asteroid")
        {
            healthBar.value -= Random.Range(0, 30);

            if(healthBar.value <= 0)
            {
                Destroy(healthBar.gameObject, 0.2f);
                Destroy(this.gameObject, 0.2f);
            }
        }
    }
}