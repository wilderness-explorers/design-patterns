using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// To store the properties of the items, that are stored in the pool.
[System.Serializable]
public class PoolItem
{
    public GameObject prefab;
    public int amount;
}

public class Pool : MonoBehaviour
{
    public static Pool singleton;

    // List of items that can be pooled.
    public List<PoolItem> items;

    // List of items that are constructed from the list of prefabs.
    public List<GameObject> pooledItems;

    private void Awake()
    {
        singleton = this;
    }

    void Start()
    {
        pooledItems = new List<GameObject>();

        foreach(PoolItem item in items)
        {
            for(int i = 0; i < item.amount; i++)
            {
                GameObject obj = Instantiate(item.prefab);
                obj.SetActive(false);
                pooledItems.Add(obj);
            }
        }
    }

    //To use the pooled items in other scripts.
    public GameObject Get(string tag)
    {
        GameObject obj = null;

        for (int i = 0; i < pooledItems.Count; i++)
        {
            if (!pooledItems[i].activeInHierarchy && pooledItems[i].tag == tag)
            {
                obj = pooledItems[i];
                return obj;
            }
        }

        return null;
    }

    void Update()
    {
        
    }
}