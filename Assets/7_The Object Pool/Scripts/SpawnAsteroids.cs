using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAsteroids : MonoBehaviour
{
    public GameObject asteroid;

    void Start()
    {
        
    }

    void Update()
    {
        if(Random.Range(0, 100) < 5)
        {
            //Instantiate(asteroid, this.transform.position + new Vector3(Random.Range(-10, 10), 0, 0), Quaternion.identity);

            GameObject asteroidPrefab = Pool.singleton.Get("Asteroid");

            if (asteroidPrefab != null)
            {
                asteroidPrefab.transform.position = this.transform.position + new Vector3(Random.Range(-10, 10), 0, 0);
                asteroidPrefab.SetActive(true);
            }
        }
    }
}
